#pragma once

#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"
#include "wav_decoder.h"

#include "esp_peripherals.h"
#include "periph_sdcard.h"
#include "periph_touch.h"
#include "periph_button.h"
#include "periph_wifi.h"
#include "input_key_service.h"

#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_wifi.h"

#define DEFAULT_I2S_STREAM_CONFIG() \
    i2s_cfg.type = AUDIO_STREAM_WRITER; \
    i2s_cfg.i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX); \
    i2s_cfg.i2s_config.sample_rate = 44100; \
    i2s_cfg.i2s_config.bits_per_sample = (i2s_bits_per_sample_t)16; \
    i2s_cfg.i2s_config.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT; \
    i2s_cfg.i2s_config.communication_format = I2S_COMM_FORMAT_I2S; \
    i2s_cfg.i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL2; \
    i2s_cfg.i2s_config.dma_buf_count = 3; \
    i2s_cfg.i2s_config.dma_buf_len = 300; \
    i2s_cfg.i2s_config.use_apll = 1; \
    i2s_cfg.i2s_config.tx_desc_auto_clear = false; \
    i2s_cfg.i2s_config.fixed_mclk = 0; \
    i2s_cfg.i2s_port = (i2s_port_t)0; \
    i2s_cfg.out_rb_size = I2S_STREAM_RINGBUFFER_SIZE; \
    i2s_cfg.task_stack = I2S_STREAM_TASK_STACK; \
    i2s_cfg.task_core = I2S_STREAM_TASK_CORE; \
    i2s_cfg.task_prio = I2S_STREAM_TASK_PRIO;

#define CONFIG_MP3_DECODER_DEFAULT() \
    mp3_cfg.out_rb_size = MP3_DECODER_RINGBUFFER_SIZE; \
    mp3_cfg.task_stack = MP3_DECODER_TASK_STACK_SIZE; \
    mp3_cfg.task_core = MP3_DECODER_TASK_CORE; \
    mp3_cfg.task_prio = MP3_DECODER_TASK_PRIO;

#define CONFIG_WAV_DECODER_DEFAULT() \
    wav_cfg.out_rb_size = WAV_DECODER_RINGBUFFER_SIZE; \
    wav_cfg.task_stack = WAV_DECODER_TASK_STACK; \
    wav_cfg.task_core = WAV_DECODER_TASK_CORE; \
    wav_cfg.task_prio = WAV_DECODER_TASK_PRIO;

#define DEFAULT_FATFS_STREAM_CONFIG() \
    fatfs_cfg.type = AUDIO_STREAM_READER; \
    fatfs_cfg.task_prio = FATFS_STREAM_TASK_PRIO; \
    fatfs_cfg.task_core = FATFS_STREAM_TASK_CORE; \
    fatfs_cfg.task_stack = FATFS_STREAM_TASK_STACK; \
    fatfs_cfg.out_rb_size = FATFS_STREAM_RINGBUFFER_SIZE; \
    fatfs_cfg.buf_sz = FATFS_STREAM_BUF_SIZE;

#define DEFAULT_HTTP_STREAM_CONFIG() \
    http_cfg.type = AUDIO_STREAM_READER; \
    http_cfg.enable_playlist_parser = true; \
    http_cfg.task_prio = HTTP_STREAM_TASK_PRIO; \
    http_cfg.task_core = HTTP_STREAM_TASK_CORE; \
    http_cfg.task_stack = HTTP_STREAM_TASK_STACK; \
    http_cfg.out_rb_size = HTTP_STREAM_RINGBUFFER_SIZE; \
    http_cfg.multi_out_num = 0;

//    http_cfg.event_handle = _http_stream_event_handle;

class AudioPlayer
{
public:
    enum AudioSource
    {
        NOSOURCE,
        FILE,
        HTTP
    };

    enum AudioFileType
    {
        NOTYPE,
        MP3,
        WAV
    };

private:
    // Single instance of AudioPlayer [Singleton]
    static AudioPlayer* instance;
    bool isInitialized;
    bool pipelineInitialized;
    bool isPlaying;
    unsigned int volume;

    AudioSource currentSource;
    AudioFileType currentFileType;

    // Function pointer called when audio has finished playing
    void(*OnFinishedPlaying)(void);
    // Current audio source
    const char* currentSourceIrl;

    // Handlers
    audio_board_handle_t board_handle;
    audio_pipeline_handle_t pipeline;
    audio_element_handle_t i2sStreamWriter, mp3Decoder, wavDecoder, fatfsStreamReader, httpStreamReader;
    esp_periph_set_handle_t periphHandle;
    audio_event_iface_handle_t audioEventHandle;

private:
    // Constructor
    AudioPlayer();
    // Destructor
    ~AudioPlayer();

    // Initializes current pipeline with source [audioSource] and filetype [audioFileType]
    void initPipeline(AudioSource audioSource, AudioFileType audioFileType);
    // Deinitializes current pipeline
    void deinitPipeline();

    static int _http_stream_event_handle(http_stream_event_msg_t *msg);

public:
    // Initializes the decoders and streamreaders
    void init(esp_periph_set_handle_t* periphHandle);
    // Deinitializes the decoders and streamreaders
    void deinit();

    // Plays audio form url/irl source
    void play(const char* source);
    // Stops audio playing form url/irl source
    void stop();

    // Sets volume of the audio
    void setVolume(int volume);
    // Increases volume by five precent
    void increaseVolume();
    // Decreases volume by five percent
    void decreaseVolume();

    // Task method checking if audio finished playing
    static void playAudioTask(void* pvParameter);
    // Checks if audio finished playing called in separate task
    void checkAudioPlayerForFinishCondition();

    // Returns the audio board handle
    audio_board_handle_t* getAudioBoardHandle();

    // Shuts down current pipeline if initialized and recreates a new pipeline with source [audioSource] and filetype [audioFileType]
    void setPipeline(AudioSource audioSource, AudioFileType audioFileType);
    // Sets the function pointer called when audio has finished playing
    void setOnFinishedPlayingCallback(void(*onFinishedPlaying)(void));

public:
    // Returns single instance of AudioPlayer [Singleton]
    static AudioPlayer* getInstance();
};