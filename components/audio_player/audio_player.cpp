#include "include/audio_player.h"

AudioPlayer* AudioPlayer::instance = nullptr;

AudioPlayer::AudioPlayer()
{
    this->isInitialized = false;
    this->pipelineInitialized = false;
    this->isPlaying = false;
    this->volume = 10;

    this->currentSource = NOSOURCE;
    this->currentFileType = NOTYPE;

    this->OnFinishedPlaying = nullptr;
}

AudioPlayer::~AudioPlayer()
{
    
}

void AudioPlayer::initPipeline(AudioSource audioSource, AudioFileType audioFileType)
{
    if(!this->pipelineInitialized)
    {
        ESP_LOGI("AudioPlayer", "Register all elements to audio pipeline");
        audio_pipeline_register(this->pipeline, (audioSource == FILE) ? this->fatfsStreamReader : this->httpStreamReader, (audioSource == FILE) ? "file" : "http");
        audio_pipeline_register(this->pipeline, (audioFileType == MP3) ? this->mp3Decoder : this->wavDecoder, (audioFileType == MP3) ? "mp3" : "wav");
        audio_pipeline_register(this->pipeline, this->i2sStreamWriter, "i2s");

        ESP_LOGI("AudioPlayer", "Linking pipeline");
        const char* pipelineArgs[] = { 
            ((audioSource == FILE) ? "file" : "http"), 
            ((audioFileType == MP3) ? "mp3" : "wav"), 
            "i2s" 
        };
        audio_pipeline_link(this->pipeline, &pipelineArgs[0], 3);

        ESP_LOGI("AudioPlayer", "Listen for all pipeline events");
        audio_pipeline_set_listener(this->pipeline, this->audioEventHandle);

        this->currentSource = audioSource;
        this->currentFileType = audioFileType;
        this->pipelineInitialized = true;
    }
}

void AudioPlayer::deinitPipeline()
{
    if(this->pipelineInitialized)
    {
        ESP_LOGI("AudioPlayer", "Stop audio_pipeline");
        audio_pipeline_terminate(this->pipeline);

        audio_pipeline_unregister(this->pipeline, (this->currentSource == FILE) ? this->fatfsStreamReader : this->httpStreamReader);
        audio_pipeline_unregister(this->pipeline, (this->currentFileType == MP3) ? this->mp3Decoder : this->wavDecoder);
        audio_pipeline_unregister(this->pipeline, this->i2sStreamWriter);

        audio_pipeline_remove_listener(this->pipeline);

        this->currentSource = NOSOURCE;
        this->currentFileType = NOTYPE;
        this->pipelineInitialized = false;
    }
}

int AudioPlayer::_http_stream_event_handle(http_stream_event_msg_t *msg)
{
    if (msg->event_id == HTTP_STREAM_RESOLVE_ALL_TRACKS) {
        return ESP_OK;
    }

    if (msg->event_id == HTTP_STREAM_FINISH_TRACK) {
        return http_stream_next_track(msg->el);
    }
    if (msg->event_id == HTTP_STREAM_FINISH_PLAYLIST) {
        return http_stream_restart(msg->el);
    }
    return ESP_OK;
}

void AudioPlayer::init(esp_periph_set_handle_t* periphHandle)
{
    if(!this->isInitialized)
    {
        // ESP_LOGI("AudioPlayer", "[1.0] Initialize peripherals management");
        // esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
        // this->periphHandle = esp_periph_set_init(&periph_cfg);
        this->periphHandle = *periphHandle;

        ESP_LOGI("AudioPlayer", "Start codec chip");
        this->board_handle = audio_board_init();
        audio_hal_ctrl_codec(this->board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);

        ESP_LOGI("AudioPlayer", "Create audio pipeline for playback");
        audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
        this->pipeline = audio_pipeline_init(&pipeline_cfg);
        mem_assert(this->pipeline);

        ESP_LOGI("AudioPlayer", "Create i2s stream to write data to codec chip");
        i2s_stream_cfg_t i2s_cfg;
        DEFAULT_I2S_STREAM_CONFIG();
        this->i2sStreamWriter = i2s_stream_init(&i2s_cfg);

        ESP_LOGI("AudioPlayer", "Create mp3 decoder to decode mp3 file");
        mp3_decoder_cfg_t mp3_cfg;
        CONFIG_MP3_DECODER_DEFAULT();
        this->mp3Decoder = mp3_decoder_init(&mp3_cfg);

        ESP_LOGI("AudioPlayer", "Create wav decoder to decode wav file");
        wav_decoder_cfg_t wav_cfg;
        CONFIG_WAV_DECODER_DEFAULT();
        this->wavDecoder = wav_decoder_init(&wav_cfg);

        ESP_LOGI("AudioPlayer", "Create fatfs stream to read data from sdcard");
        char* url = NULL;
        fatfs_stream_cfg_t fatfs_cfg;
        DEFAULT_FATFS_STREAM_CONFIG();
        this->fatfsStreamReader = fatfs_stream_init(&fatfs_cfg);
        audio_element_set_uri(this->fatfsStreamReader, url);

        ESP_LOGI("AudioPlayer", "Create http stream to read data");
        http_stream_cfg_t http_cfg;
        http_cfg.event_handle = (http_stream_event_handle_t)&AudioPlayer::_http_stream_event_handle;
        DEFAULT_HTTP_STREAM_CONFIG();
        this->httpStreamReader = http_stream_init(&http_cfg);

        ESP_LOGI("AudioPlayer", "Set up event listener");
        audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
        this->audioEventHandle = audio_event_iface_init(&evt_cfg);

        // ESP_LOGI("AudioPlayer", "Listen for all pipeline events");
        // audio_pipeline_set_listener(this->pipeline, this->audioEventHandle);

        this->isInitialized = true;
        setVolume(this->volume);
    }
    else
        ESP_LOGE("AudioPlayer", "Audioplayer already Initialized!");
}

void AudioPlayer::deinit()
{
    if(this->isInitialized)
    {
        deinitPipeline();

        // /* Terminate the pipeline before removing the listener */
        // audio_pipeline_remove_listener(this->pipeline);

        /* Stop all peripherals before removing the listener */
        esp_periph_set_stop_all(this->periphHandle);
        audio_event_iface_remove_listener(esp_periph_set_get_event_iface(this->periphHandle), this->audioEventHandle);

        /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
        audio_event_iface_destroy(this->audioEventHandle);

        /* Release all resources */
        audio_pipeline_deinit(this->pipeline);
        audio_element_deinit((this->currentSource == FILE) ? this->fatfsStreamReader : this->httpStreamReader);
        audio_element_deinit(this->i2sStreamWriter);
        audio_element_deinit((this->currentFileType == MP3) ? this->mp3Decoder : this->wavDecoder);

        this->isInitialized = false;
    }
    else
        ESP_LOGE("AudioPlayer", "Audioplayer not Initialized!");
}

void AudioPlayer::play(const char* source)
{
    if(this->isInitialized && this->pipelineInitialized && !this->isPlaying)
    {
        this->currentSourceIrl = source;
        audio_element_set_uri((this->currentSource == FILE) ? this->fatfsStreamReader : this->httpStreamReader, this->currentSourceIrl);
        audio_pipeline_reset_ringbuffer(this->pipeline);
        audio_pipeline_reset_elements(this->pipeline);
        audio_pipeline_change_state(this->pipeline, AEL_STATE_INIT);
        audio_pipeline_run(this->pipeline);

        this->isPlaying = true;
        //xTaskCreate(&AudioPlayer::playAudioTask, "playtask", 1024, NULL, 10, NULL);
        //xTaskCreate(&AudioPlayer::playAudioTask, "playtask", 2048, NULL, 10, NULL);
        xTaskCreate(&AudioPlayer::playAudioTask, "playtask", 4096, NULL, 10, NULL);
    }
    else if(!this->isInitialized && !this->pipelineInitialized)
        ESP_LOGE("AudioPlayer", "Audioplayer and pipeline not Initialized!");
    else if(!this->isInitialized)
        ESP_LOGE("AudioPlayer", "Audioplayer not Initialized!");
    else if(!this->pipelineInitialized)
        ESP_LOGE("AudioPlayer", "Pipeline not Initialized!");
    else if(!this->isPlaying)
        ESP_LOGE("AudioPlayer", "Audio already playing!");
}

void AudioPlayer::stop()
{
    if(this->isPlaying)
    {
        this->isPlaying = false;
        audio_pipeline_pause(this->pipeline);
    }
    else
        ESP_LOGE("AudioPlayer", "Audioplayer not Playing!");
}

void AudioPlayer::setVolume(int volume)
{
    if(this->isInitialized)
    {
        if(volume > 100)
            this->volume = 100;
        else if(volume < 0)
            this->volume = 0;
        else
            this->volume = volume;
        
        audio_hal_set_volume(this->board_handle->audio_hal, this->volume);
    }
    else
        ESP_LOGE("AudioPlayer", "Audioplayer not Initialized!");
}

void AudioPlayer::increaseVolume()
{
    setVolume(this->volume + 5);
}

void AudioPlayer::decreaseVolume()
{
    setVolume(this->volume - 5);
}

void AudioPlayer::playAudioTask(void* pvParameter)
{
    AudioPlayer::getInstance()->checkAudioPlayerForFinishCondition();
}

void AudioPlayer::checkAudioPlayerForFinishCondition()
{
    while(this->isPlaying)
    {
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(this->audioEventHandle, &msg, portMAX_DELAY);
        if (ret != ESP_OK)
        {
            ESP_LOGE("AudioPlayer", "[ * ] Event interface error : %d", ret);
            continue;
        }
        if (msg.source == (void*)this->i2sStreamWriter && msg.cmd == AEL_MSG_CMD_REPORT_STATUS)
        {
            audio_element_state_t el_state = audio_element_get_state(this->i2sStreamWriter);
            if (el_state == AEL_STATE_FINISHED || !this->isPlaying)
            {
                ESP_LOGI("AudioPlayer", "Finished sample");
                audio_pipeline_pause(this->pipeline);
                this->isPlaying = false;
                if(this->OnFinishedPlaying != nullptr)
                    this->OnFinishedPlaying();
                break;
            }
        }
        vTaskDelay(100 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

audio_board_handle_t* AudioPlayer::getAudioBoardHandle()
{
    return &this->board_handle;
}

void AudioPlayer::setPipeline(AudioSource audioSource, AudioFileType audioFileType)
{
    if(this->currentSource == audioSource && this->currentFileType == audioFileType)
        return;
    
    if(this->isInitialized)
    {
        if(this->pipelineInitialized)
        {
            stop();
            //Wait a bit to allow task and audio to stop
            vTaskDelay(10 / portTICK_RATE_MS);
            deinitPipeline();
        }
        if(audioSource != NOSOURCE && audioFileType != NOTYPE)
            initPipeline(audioSource, audioFileType);
    }
    else
        ESP_LOGE("AudioPlayer", "Audioplayer not Initialized!");
}

void AudioPlayer::setOnFinishedPlayingCallback(void(*onFinishedPlaying)(void))
{
    this->OnFinishedPlaying = onFinishedPlaying;
}

AudioPlayer* AudioPlayer::getInstance()
{
    if(AudioPlayer::instance == nullptr)
        AudioPlayer::instance = new AudioPlayer();
    return AudioPlayer::instance;
}