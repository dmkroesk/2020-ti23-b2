#include "buttonInput.h"
#include <stdio.h>

#define I2C_ADDRESS 0x20;
#define SDA_PIN		18;
#define SCL_PIN		23;

static const char* TAG = "I2C_MCP23017_tactilebuttons";
mcp23017_t mcp23017;
SemaphoreHandle_t xMutex;
uint8_t TACTILE_BUTTON_STATES = 2;


//Return bitmask from pins
uint8_t tactilebuttons_get_pressed() {
    return TACTILE_BUTTON_STATES;
}

//Print the pins that are pressed by a user on GPIO 1 - 8.
void tactilebuttons_print_pressed() {
     for (int i = 0; i < 8; i++)
        if (TACTILE_BUTTON_STATES & (1<<i))
                ESP_LOGI(TAG, "Bit %d is on! Button is off", i);
        else
                ESP_LOGI(TAG, "Bit %d is off! Button is pressed", i);
}

//Reads all the pins
void tactilebuttons_task_read(void* pvParameters)
{
    while (1) {
	   vTaskDelay(100 / portTICK_RATE_MS);
	   uint8_t states = 0x00;
	   xSemaphoreTake( xMutex, portMAX_DELAY );
	   mcp23017_read_register(&mcp23017, MCP23017_GPIO, GPIOB, &states);
	   xSemaphoreGive( xMutex );
	   TACTILE_BUTTON_STATES = states;
	   if (0) //Logging disabled.
        ESP_LOGI(TAG, "GPIO register B states: %d", states);
    }
    vTaskDelete(NULL);
}

//Initializes all the buttons
void tactilebuttons_init(SemaphoreHandle_t* mutex) {

	mcp23017.i2c_addr = I2C_ADDRESS;
	mcp23017.sda_pin = SDA_PIN;
	mcp23017.scl_pin = SCL_PIN;
	mcp23017.sda_pullup_en = GPIO_PULLUP_ENABLE;
	mcp23017.scl_pullup_en = GPIO_PULLUP_ENABLE;
	ESP_ERROR_CHECK(mcp23017_init(&mcp23017));

	xMutex = *mutex;
	mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOA, 0x00); // full port on OUTPUT
	mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOB, 0xFF); // full port on INPUT
	mcp23017_write_register(&mcp23017, MCP23017_GPPU, GPIOB, 0xFF); // full port on INPUT
	xTaskCreate(tactilebuttons_task_read, "mcp23017_task_read", 1024, NULL, 10,NULL);
}

//Returns 1 if pin if specific pin is pressed
uint8_t tactilebuttons_getPin(int pin)
{
    return TACTILE_BUTTON_STATES & (1 << pin) ? 0 : 1;
}

