#pragma once
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "mcp23017.h"

#ifdef __cplusplus
extern "C" {
#endif

	// all of your legacy C code here
	uint8_t tactilebuttons_get_pressed();
	void tactilebuttons_task_read(void*);
	void tactilebuttons_init(SemaphoreHandle_t* mutex);
	uint8_t tactilebuttons_getPin(int pin);



#ifdef __cplusplus
}
#endif
