#include "include/tone_detector.h"

ToneDetector* ToneDetector::instance = nullptr;

ToneDetector::ToneDetector()
{
    this->isInitialized = false;
    this->pipelineInitialized = false;
    this->isListening = false;

    this->detectionFrequencies = std::vector<int>();

    this->OnToneDetected = nullptr;
}

ToneDetector::~ToneDetector()
{
    
}

void ToneDetector::initPipeline()
{
    if(!this->pipelineInitialized)
    {
        ESP_LOGI("ToneDetector", "Register all elements to audio pipeline");
        audio_pipeline_register(this->pipeline, this->i2sStreamReader, "i2s");
        audio_pipeline_register(this->pipeline, this->rspFilter, "filter");
        audio_pipeline_register(this->pipeline, this->rawStreamReader, "raw");

        ESP_LOGI("AudioPlayer", "Linking pipeline");
        const char* pipelineArgs[] = { 
            "i2s", 
            "filter", 
            "raw" 
        };
        audio_pipeline_link(this->pipeline, &pipelineArgs[0], 3);

        int goertzelNDirection = this->detectionFrequencies.size();

        this->goertzelFilter = goertzel_malloc(goertzelNDirection); // Alloc mem
        // Apply configuration for all filters
        for (int i = 0; i < goertzelNDirection; i++) {
            goertzel_data_t* currFilter = this->goertzelFilter[i];
            currFilter->samples = GOERTZEL_BUFFER_LENGTH;
            currFilter->sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
            currFilter->target_frequency = this->detectionFrequencies[i];
            currFilter->goertzel_cb = &ToneDetector::goertzelCallback;
        }

        // Initialize goertzel filters
	    goertzel_init_configs(this->goertzelFilter, goertzelNDirection);

        this->pipelineInitialized = true;
    }
}

void ToneDetector::deinitPipeline()
{
    if(this->pipelineInitialized)
    {
        ESP_LOGI("ToneDetector", "Destroy goertzel");
        goertzel_free(this->goertzelFilter);

        ESP_LOGI("ToneDetector", "Stop audio_pipeline and release all resources");
        audio_pipeline_terminate(this->pipeline);
        
        audio_pipeline_remove_listener(this->pipeline);

        audio_pipeline_unregister(this->pipeline, this->i2sStreamReader);
        audio_pipeline_unregister(this->pipeline, this->rspFilter);
        audio_pipeline_unregister(this->pipeline, this->rawStreamReader);

        this->pipelineInitialized = false;
    }
}

void ToneDetector::init(std::vector<int> frequencies)
{
    if(!this->isInitialized)
    {
        this->detectionFrequencies = frequencies;
        
        //this->board_handle = *board_handle;
        ESP_LOGI("ToneDetector", "Start codec chip");
        this->board_handle = audio_board_init();
        audio_hal_ctrl_codec(this->board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_BOTH, AUDIO_HAL_CTRL_START);

        ESP_LOGI("ToneDetector", "reate audio pipeline for recording");
        audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
        this->pipeline = audio_pipeline_init(&pipeline_cfg);
        mem_assert(this->pipeline);

        ESP_LOGI("ToneDetector", "Create i2s stream to read audio data from codec chip");
        i2s_stream_cfg_t i2s_cfg;
        DEFAULT_I2S_STREAM_CONFIG();
        i2s_cfg.i2s_config.sample_rate = AUDIO_SAMPLE_RATE;
        i2s_cfg.type = AUDIO_STREAM_READER;
        this->i2sStreamReader = i2s_stream_init(&i2s_cfg);

        ESP_LOGI("ToneDetector", "Create filter to resample audio data");
        rsp_filter_cfg_t rsp_cfg;
        RESAMPLE_FILTER_CONFIG_DEFAULT();
        rsp_cfg.src_rate = AUDIO_SAMPLE_RATE;
        rsp_cfg.src_ch = 2;
        rsp_cfg.dest_rate = GOERTZEL_SAMPLE_RATE_HZ;
        rsp_cfg.dest_ch = 1;
        this->rspFilter = rsp_filter_init(&rsp_cfg);

        ESP_LOGI("ToneDetector", "Create raw to receive data");
        raw_stream_cfg_t raw_cfg;
        raw_cfg.out_rb_size = 8 * 1024;
        raw_cfg.type = AUDIO_STREAM_READER;
        this->rawStreamReader = raw_stream_init(&raw_cfg);

        this->isInitialized = true;
    }
    else
        ESP_LOGE("ToneDetector", "Tonedetector already Initialized!");
}

void ToneDetector::deinit()
{
    if(this->isInitialized)
    {
        audio_pipeline_deinit(pipeline);
        audio_element_deinit(this->i2sStreamReader);
        audio_element_deinit(this->rspFilter);
        audio_element_deinit(this->rawStreamReader);

        this->isInitialized = false;
    }
    else
        ESP_LOGE("ToneDetector", "Tonedetector not Initialized!");
}


void ToneDetector::startListening()
{
    if(!this->isListening)
    {
        this->isListening = true;

        initPipeline();
        audio_pipeline_run(this->pipeline);
        xTaskCreate(&ToneDetector::listen, "listentask", 4096, NULL, 10, NULL);
    }
    else
        ESP_LOGE("ToneDetector", "Tonedetector already Listening!");
}

void ToneDetector::stopListening()
{
    if(this->isListening)
    {
        this->isListening = false;
        // Wait a bit for the listeningtask to finish
        vTaskDelay(100 / portTICK_RATE_MS);
    }
    else
        ESP_LOGE("ToneDetector", "Tonedetector not Listening!");
}

void ToneDetector::setOnToneDetectedCallback(void(*onToneDetected)(int, float))
{
    this->OnToneDetected = onToneDetected;
}

void ToneDetector::listenForTone()
{
    bool noError = true;
    int16_t *raw_buff = (int16_t *)malloc(GOERTZEL_BUFFER_LENGTH * sizeof(short));
    if (raw_buff == NULL) {
        ESP_LOGE("ToneDetector", "Memory allocation failed!");
        noError = false;
    }

    while (noError && this->isListening) {
        raw_stream_read(this->rawStreamReader, (char*)raw_buff, GOERTZEL_BUFFER_LENGTH * sizeof(short));
		
		// process Goertzel Samples
		goertzel_proces(this->goertzelFilter, this->detectionFrequencies.size(), raw_buff, GOERTZEL_BUFFER_LENGTH);
        
        vTaskDelay(400 / portTICK_RATE_MS);
    }

	if (raw_buff != NULL) {
		free(raw_buff);
		raw_buff = NULL;
	}

    deinitPipeline();
}

void ToneDetector::listen(void* pvParameters)
{
    ToneDetector* toneDetector = ToneDetector::getInstance();
    toneDetector->listenForTone();
    vTaskDelete(NULL);
}

void ToneDetector::goertzelCallback(goertzel_data_t* filter, float result) 
{
	goertzel_data_t* filt = (goertzel_data_t*)filter;
	float logVal = 10.0f * log10f(result);
	
	// Detection filter. Only above 40 dB(A)
	if (logVal > 40.0f)
    {
		ESP_LOGI("ToneDetector", "[Goertzel] Callback Freq: %d Hz amplitude: %.2f", filt->target_frequency, logVal);
        ToneDetector::getInstance()->toneDetected(filt->target_frequency, logVal);
    }
}

void ToneDetector::toneDetected(int frequency, float decibles)
{
    if(this->OnToneDetected != nullptr)
        this->OnToneDetected(frequency, decibles);
}

ToneDetector* ToneDetector::getInstance()
{
    if(ToneDetector::instance == nullptr)
        ToneDetector::instance = new ToneDetector();
    return ToneDetector::instance;
}