#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "goertzel.h"
#include <math.h>

#include <vector>

#define DEFAULT_I2S_STREAM_CONFIG() \
    i2s_cfg.type = AUDIO_STREAM_WRITER; \
    i2s_cfg.i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX); \
    i2s_cfg.i2s_config.sample_rate = 44100; \
    i2s_cfg.i2s_config.bits_per_sample = (i2s_bits_per_sample_t)16; \
    i2s_cfg.i2s_config.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT; \
    i2s_cfg.i2s_config.communication_format = I2S_COMM_FORMAT_I2S; \
    i2s_cfg.i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL2; \
    i2s_cfg.i2s_config.dma_buf_count = 3; \
    i2s_cfg.i2s_config.dma_buf_len = 300; \
    i2s_cfg.i2s_config.use_apll = 1; \
    i2s_cfg.i2s_config.tx_desc_auto_clear = false; \
    i2s_cfg.i2s_config.fixed_mclk = 0; \
    i2s_cfg.i2s_port = (i2s_port_t)0; \
    i2s_cfg.out_rb_size = I2S_STREAM_RINGBUFFER_SIZE; \
    i2s_cfg.task_stack = I2S_STREAM_TASK_STACK; \
    i2s_cfg.task_core = I2S_STREAM_TASK_CORE; \
    i2s_cfg.task_prio = I2S_STREAM_TASK_PRIO;

#define RESAMPLE_FILTER_CONFIG_DEFAULT() \
    rsp_cfg.src_rate = 44100; \
    rsp_cfg.src_ch = 2; \
    rsp_cfg.dest_rate = 48000; \
    rsp_cfg.dest_ch = 2; \
    rsp_cfg.sample_bits = 16; \
    rsp_cfg.mode = RESAMPLE_DECODE_MODE; \
    rsp_cfg.max_indata_bytes = RSP_FILTER_BUFFER_BYTE; \
    rsp_cfg.out_len_bytes = RSP_FILTER_BUFFER_BYTE; \
    rsp_cfg.type = ESP_RESAMPLE_TYPE_AUTO; \
    rsp_cfg.complexity = 0; \
    rsp_cfg.down_ch_idx = 0; \
    rsp_cfg.out_rb_size = RSP_FILTER_RINGBUFFER_SIZE; \
    rsp_cfg.task_stack = RSP_FILTER_TASK_STACK; \
    rsp_cfg.task_core = RSP_FILTER_TASK_CORE; \
    rsp_cfg.task_prio = RSP_FILTER_TASK_PRIO;

#define GOERTZEL_SAMPLE_RATE_HZ 8000	// Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 100	// Block length in [ms]
#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000)

#define AUDIO_SAMPLE_RATE 48000			// Default sample rate in [Hz]

class ToneDetector
{
private:
    // Single instance of ToneDetector [Singleton]
    static ToneDetector* instance;
    bool isInitialized;
    bool pipelineInitialized;
    bool isListening;

    std::vector<int> detectionFrequencies;

    // Function pointer called when a tone was detected and passes the frequency and decibels that was detected
    void(*OnToneDetected)(int, float);

    // Handlers
    audio_board_handle_t board_handle;
    audio_pipeline_handle_t pipeline;
    audio_element_handle_t i2sStreamReader, rspFilter, rawStreamReader;

    // Groetzel
    goertzel_data_t** goertzelFilter;

private:
    // Constructor
    ToneDetector();
    // Destructor
    ~ToneDetector();

    // Initializes current pipeline
    void initPipeline();
    // Deinitializes current pipeline
    void deinitPipeline();

public:
    // Initializes the decoders and streamreaders
    void init(std::vector<int> frequencies);
    // Deinitializes the decoders and streamreaders
    void deinit();

    // Start listening for tones
    void startListening();
    // Stop listening for tones
    void stopListening();

    // Sets the function pointer called when a tone is detected
    void setOnToneDetectedCallback(void(*onToneDetected)(int, float));

private:
    void listenForTone();
    static void listen(void* pvParameters);

    void toneDetected(int frequency, float decibles);
    static void goertzelCallback(goertzel_data_t* filter, float result);
    
public:
    // Returns single instance of ToneDetector [Singleton]
    static ToneDetector* getInstance();
};