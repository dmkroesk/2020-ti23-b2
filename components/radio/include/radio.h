#pragma once

#include <vector>
#include <string>

#include "audio_player.h"

class Radio
{
private:
    AudioPlayer* audioPlayer;

    const char* channels[4] = {
        "https://icecast.omroep.nl/3fm-serioustalent-mp3",
        "http://server-67.stream-server.nl:8400",  
        "https://icecast.omroep.nl/3fm-alternative-mp3",
        "https://icecast.omroep.nl/funx-dance-bb-mp3"
    };

public:
    Radio();
    ~Radio();

    void setChannel(unsigned int channel);
};