#include "include/radio.h"

Radio::Radio()
{
    this->audioPlayer = AudioPlayer::getInstance();
}

Radio::~Radio()
{

}

void Radio::setChannel(unsigned int channel)
{
    if(channel < 4)
    {
        this->audioPlayer->stop();
        this->audioPlayer->setPipeline(AudioPlayer::AudioSource::HTTP, AudioPlayer::AudioFileType::MP3);
        this->audioPlayer->play(this->channels[channel]);
    }
    else
        ESP_LOGE("Radio", "Channel does not exist!");
}