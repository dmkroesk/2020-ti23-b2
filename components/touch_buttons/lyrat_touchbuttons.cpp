#include "include/lyrat_touchbuttons.h"

const char* TouchButtonHandler::TAG = "TouchButtonHandler";

TouchButtonHandler::TouchButtonHandler(audio_board_handle_t* handle, esp_periph_set_handle_t *set)
{
    ESP_LOGI(TAG, "[ 3 ] Create and start input key service");
    input_key_service_info_t input_key_info[] = INPUT_KEY_DEFAULT_INFO();
    periph_service_handle_t input_ser = input_key_service_create(*set);
    input_key_service_add_key(input_ser, input_key_info, INPUT_KEY_NUM);
    periph_service_set_callback(input_ser, &TouchButtonHandler::input_key_service_cb, (void*)handle);
}

esp_err_t TouchButtonHandler::input_key_service_cb(periph_service_handle_t handle, periph_service_event_t* evt, void* ctx)
{
    audio_board_handle_t board_handle = (audio_board_handle_t)ctx;

    if (evt->type == INPUT_KEY_SERVICE_ACTION_CLICK_RELEASE) 
    {
        ESP_LOGI(TAG, "input key id is %d", (int)evt->data);
        switch ((int)evt->data) 
        {
            case INPUT_KEY_USER_ID_PLAY:
            {
                ESP_LOGI(TAG, "[Play] pressed play button");
                break;
            }
            case INPUT_KEY_USER_ID_SET:
            {
                ESP_LOGI(TAG, "[Set] pressed set button");
                break;
            }
            case INPUT_KEY_USER_ID_VOLUP:
            {
                ESP_LOGI(TAG, "[Vol+] pressed volume up button");
                AudioPlayer::getInstance()->increaseVolume();
                break;
            }
            case INPUT_KEY_USER_ID_VOLDOWN:
            {
                ESP_LOGI(TAG, "[Vol-] pressed volume down button");
                AudioPlayer::getInstance()->decreaseVolume();
                break;
            }
        }
    }

    return ESP_OK;
}
