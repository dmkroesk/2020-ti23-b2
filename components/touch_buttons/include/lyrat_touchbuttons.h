#pragma once
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "i2s_stream.h"
#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"
#include "input_key_service.h"

#include "audio_mem.h"
#include "raw_stream.h"
#include "filter_resample.h"

#include "audio_player.h"

class TouchButtonHandler
{
private:
    static const char* TAG;
public:
    TouchButtonHandler(audio_board_handle_t *handle, esp_periph_set_handle_t* set);

private:
    static esp_err_t input_key_service_cb(periph_service_handle_t handle, periph_service_event_t* evt, void* ctx);
};

