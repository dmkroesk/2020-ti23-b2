set(COMPONENT_REQUIRES audio_player audio_pipeline audio_stream audio_board esp-adf-libs esp_peripherals input_key_service nvs_flash)
set(COMPONENT_PRIV_REQUIRES audio_player audio_pipeline audio_stream audio_board esp-adf-libs esp_peripherals input_key_service nvs_flash)

set(COMPONENT_SRCS "lyrat_touchbuttons.cpp")
set(COMPONENT_ADD_INCLUDEDIRS "include")

register_component()