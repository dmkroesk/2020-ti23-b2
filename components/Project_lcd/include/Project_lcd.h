#pragma once

// __CR__ [PSMG]: Zoals eerder benoemd dit moet je op een andere manier implementeren.
#include "i2c-lcd1602.h"
#include "smbus.h"
#include <string>
//#include "../../i2c_tdriver/include/i2c_tdriver.h"

/*
*	Author: Ties & Marleen
*/

class Project_lcd
{
private:
    i2c_lcd1602_info_t * lcd_info;
	smbus_info_t* smbus_info;
public:
	
	Project_lcd(i2c_port_t i2c_num, i2c_address_t slaveAddress);
	~Project_lcd();
	
	void send_text(char* text);
    void clear();
    //move cursor to different place on LCD
    void move_cursor(uint8_t index, uint8_t row);
    //Write one character to LCD
    void write_char(uint8_t c);
    //Write on specific place
    void write_on_this_spot(const char* text, uint8_t index, uint8_t row);
    //turn off screen
    void disable_screen();
    //turn on screen
    void enable_screen();
    //scrolls text one place to the left
    void scroll_left();
    //scrolls text one place to the right
    void scroll_right();
    //make custom character
    void define_custom_char(uint8_t* character, i2c_lcd1602_custom_index_t indexOfCC, std::string name);
    //don't scrolls when text don't fit on the screen
    void disable_autoscroll();
    //scrolls when text don't fit on the screen
    void enable_autoscroll();

};
