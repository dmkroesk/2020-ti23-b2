﻿// __CR__ [PSMG]: verkeerde manier van includen. Gebruik #include "project_lcd.h"
#include "Project_lcd.h"

#define LCD_NUM_ROWS			 4
#define LCD_NUM_COLUMNS			 40
#define LCD_NUM_VIS_COLUMNS		 20


Project_lcd::Project_lcd(i2c_port_t i2c_num, i2c_address_t slaveAddress)
{
	
    smbus_info = smbus_malloc();
	//smbus_init(smbus_info, I2C_MASTER_NUM, 0x27);
	smbus_init(smbus_info, i2c_num, slaveAddress);
	smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);
	    
    // Set up the LCD1602 device with backlight off
	lcd_info = i2c_lcd1602_malloc();
    i2c_lcd1602_init(lcd_info, smbus_info, true, LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VIS_COLUMNS);
}

//Destructor
Project_lcd::~Project_lcd() {
	smbus_free(&smbus_info);
	i2c_lcd1602_free(&lcd_info);
}

//Writes a string to the LCD
void Project_lcd::send_text(char* text) {
    //_wait_for_user();
    i2c_lcd1602_home(lcd_info);
    i2c_lcd1602_write_string(lcd_info, text);
}

//Clears the entire LCD
void Project_lcd::clear() {
    i2c_lcd1602_clear(lcd_info);
}

//Moves the cursor to index (x) and row (y)
void Project_lcd::move_cursor(uint8_t index, uint8_t row) {
    i2c_lcd1602_move_cursor(lcd_info, index, row);
}

//Write a char to the display
void Project_lcd::write_char(uint8_t c) {
    i2c_lcd1602_write_char(lcd_info, c);
}

//Sets the cursor to index, row. Writes text from there
void Project_lcd::write_on_this_spot(const char* text, uint8_t index, uint8_t row) {
    move_cursor(index, row);
    i2c_lcd1602_write_string(lcd_info, text);
}

//Turn the screen off
void Project_lcd::disable_screen() {
    i2c_lcd1602_set_display(lcd_info, false);
}

//Turn the screen on
void Project_lcd::enable_screen() {
    i2c_lcd1602_set_display(lcd_info, true);
}

//Scroll all the text 1 char to the left
void Project_lcd::scroll_left() {
    i2c_lcd1602_scroll_display_left(lcd_info);
}

//Scroll all the text 1 char to the right
void Project_lcd::scroll_right() {
    i2c_lcd1602_scroll_display_right(lcd_info);
}

//Create a custom character
void Project_lcd::define_custom_char(uint8_t* character, i2c_lcd1602_custom_index_t indexOfCC, std::string name) {
    i2c_lcd1602_define_char(lcd_info, indexOfCC, character);
}

//Disables writing from one line to another if the string is too long
void Project_lcd::disable_autoscroll() {
    i2c_lcd1602_set_auto_scroll(lcd_info, false);
}

//Enables writing from one line to another if the string is too long
void Project_lcd::enable_autoscroll() {
    i2c_lcd1602_set_auto_scroll(lcd_info, true);
}
