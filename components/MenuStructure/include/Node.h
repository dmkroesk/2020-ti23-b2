
/*	The defenition of a Node for the menu structure
*	Author: Jasper
*/

#pragma once
#include <vector>
#include <iostream>
#include <string>

using namespace std;

class Node
{
private:
	void (*OnEnter)(void);
	void (*OnEscape)(void);

public: 
	Node(Node* p, vector<string>lines, void (*OnEnter)(void), void (*OnEscape)(void));
	Node* parent;
	vector <string> lines;
	void SafeOnEnter();
	void SafeOnEscape();
	void Add(Node* n);
	vector<Node*> children;
};
