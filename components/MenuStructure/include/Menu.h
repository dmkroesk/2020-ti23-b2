
/*	The Menu structure
 *	Author: Jasper
 */

#include "Node.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include "Project_lcd.h"

enum Inputs
{
	next,
	previous,
	confirm,
	escape
};

class Menu
{
private:
	Node* current;
	Project_lcd* lcd;
public:
	Menu(Node* mainNode, Project_lcd* newLcd);
	void HandleInput(Inputs i);
	void Cycle(bool next);
	void Confirm();
	void Escape();
	void Show();
	int GetCurrentIndex(Node* parent, Node* current);
};
