set(COMPONENT_REQUIRES project_lcd)
set(COMPONENT_PRIV_REQUIRES project_lcd)

set(COMPONENT_SRCS "Node.cpp" "Menu.cpp")
set(COMPONENT_ADD_INCLUDEDIRS include)

register_component()