#include "include/Menu.h"
/*	
*	Author: Jasper
*/

//Constructor
Menu::Menu(Node* mainNode, Project_lcd* newLcd)
{
	current = mainNode;	
	lcd = newLcd;
	Show();
}

//Convert an input to the Inputs enum and handle it
void Menu::HandleInput(Inputs i)
{
	switch (i)
	{
	case Inputs::next:
		Cycle(true);
		break;
	case previous:
		Cycle(false);
		break;
	case confirm:
		Confirm();
		break;
	case escape:
		Escape();
		break;
	default:
		break;
	}
}

//Go to the next or previouse menu node
void Menu::Cycle(bool next)
{
	Node* parent;
	parent = current->parent;
	
	if (parent)
	{
		int index = GetCurrentIndex(parent, current);
		int currentIndex = index;
		
		if (next)
		{
			index++;
		}
		else
		{
			index--;
		}
		
		index = index % parent->children.size();

		if (index != currentIndex)
		{
			current = parent->children[index];
			Show();
		}
	}
}

//Go to the child menu node or just confirm
void Menu::Confirm()
{
	if (current->children.size() > 0)
	{
		current = current->children[0];
		current->SafeOnEnter();
		Show();
	}
	else
	{
		current->SafeOnEnter();
	}
}

//Go to the parent menu node or leave another function
void::Menu::Escape()
{
	if (current->parent)
	{
		current->SafeOnEscape();
		current = current->parent;
		Show();
		current->SafeOnEnter();
	}
	else
	{
		cout << "ERROR: No parent to jump into" << endl;
	}
}

//Print the lines to show in the console
//If possible print the lines to the LCD
void Menu::Show()
{
	for (size_t i = 0; i < current->lines.size(); i++)
	{
		cout << current->lines[i] << endl;
	}

	if (lcd)
	{
		lcd->clear();
		
		for (uint8_t i = 0; i < 4; i++)
		{
			string str = current->lines[i];
			char* cstr = (char*)str.c_str();
			lcd->write_on_this_spot(cstr, 0, i);
		}
	}
}

//Returns the specified index in the Node Vector (a list) of the parent
int Menu::GetCurrentIndex(Node* parent, Node* current)
{
	auto element = find(parent->children.begin(), parent->children.end(), current);
	return distance(parent->children.begin(), element);
}