#include "include/Node.h"

// Author: Jasper

// Constructor
Node::Node(Node* p, std::vector<std::string> lcdLines, void (*OnEnterFunction)(void), void (*OnEscapeFunction)(void))
{
	parent = p;
	
	if (p != nullptr)
		p->Add(this);

	lines = lcdLines;
	OnEnter = OnEnterFunction;
	OnEscape = OnEscapeFunction;
}

// Prevents Nullpointer exceptions
void Node::SafeOnEnter()
{
	if (OnEnter)
	{
		OnEnter();
	}
	else
	{
		std::cout << "ERROR: No OnEnter function. OnEnter = NULL" << '\n';
	}
}

// Prevents Nullpointer exceptions
void Node::SafeOnEscape()
{
	if (OnEscape)
	{
		OnEscape();
	}
	else
	{
		std::cout << "ERROR: No OnEscape function. OnEscape = NULL" << '\n';
	}
}

// Adds a child node to this node
void Node::Add(Node* n)
{
	children.push_back(n);
}