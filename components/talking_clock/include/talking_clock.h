#pragma once

//#include "../../nvs_flash/include/nvs_flash.h"
#include "esp_peripherals.h"
#include "periph_wifi.h"
#include <sys/time.h>
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include <string>
#include <iostream>

#include "audio_player.h"
#include "sntp_sync.h"

#define TALKING_CLOCK_ITSNOW_INDEX 0
#define TALKING_CLOCK_HOUR_INDEX 1
#define TALKING_CLOCK_MINUTEN_INDEX 2
#define TALKING_CLOCK_AND_INDEX 3

class TalkingClock 
{
public:
	struct Time
	{
		int hours;
		int minutes;
		int seconds;
	};

private:
	static TalkingClock* instance;

	const char* talkingClockFilesMinutes[18] = {
		"/sdcard/number12.mp3",
		"/sdcard/number1.mp3",
		"/sdcard/number2.mp3",
		"/sdcard/number3.mp3",
		"/sdcard/number4.mp3",
		"/sdcard/number5.mp3",
		"/sdcard/number6.mp3",
		"/sdcard/number7.mp3",
		"/sdcard/number8.mp3",
		"/sdcard/number9.mp3",
		"/sdcard/number10.mp3",
		"/sdcard/number11.mp3",
		"/sdcard/number12.mp3",
		"/sdcard/number13.mp3",
		"/sdcard/number14.mp3"
	};

	const char* talkingClockFilesTenMinutes[5] = {
		"/sdcard/number10.mp3",
		"/sdcard/number20.mp3",
		"/sdcard/number30.mp3",
		"/sdcard/number40.mp3",
		"/sdcard/number50.mp3"
	};

	const char* talkingClockFilesSpecial[4] = {
		"/sdcard/hetisnu.mp3",
		"/sdcard/uur.mp3",
		"/sdcard/minuten.mp3",
		"/sdcard/en.mp3",
	};

	// const char* talkingClockFilesMinutes[18] = {
	// 	"/sdcard/n12.wav",
	// 	"/sdcard/n1.wav",
	// 	"/sdcard/n2.wav",
	// 	"/sdcard/n3.wav",
	// 	"/sdcard/n4.wav",
	// 	"/sdcard/n5.wav",
	// 	"/sdcard/n6.wav",
	// 	"/sdcard/n7.wav",
	// 	"/sdcard/n8.wav",
	// 	"/sdcard/n9.wav",
	// 	"/sdcard/n10.wav",
	// 	"/sdcard/n11.wav",
	// 	"/sdcard/n12.wav",
	// 	"/sdcard/n13.wav",
	// 	"/sdcard/n14.wav"
	// };

	// const char* talkingClockFilesTenMinutes[5] = {
	// 	"/sdcard/n10.wav",
	// 	"/sdcard/n20.wav",
	// 	"/sdcard/n30.wav",
	// 	"/sdcard/n40.wav",
	// 	"/sdcard/n50.wav"
	// };

	// const char* talkingClockFilesSpecial[4] = {
	// 	"/sdcard/hetisnu.wav",
	// 	"/sdcard/cuur.wav",
	// 	"/sdcard/cminuten.wav",
	// 	"/sdcard/cen.wav",
	// };

	QueueHandle_t talkingQueue;
	TimerHandle_t timerHandle;

	void(*OnTimerUpdate)(Time time);
	Time currentTime;
	//int counter;

	AudioPlayer* audioPlayer;

private:
	TalkingClock();
public:
	void start();
	void stop();
	void update();
	void buildQueue();
	void speak();
	void startSpeaking();

	void setValues();

	void setOnTimerUpdate(void(*onTimerUpdate)(Time time));

	static void timerCallback(TimerHandle_t xTimer);
	static TalkingClock* getInstance();

private:
	void init();
	void syncTime();
	void setTime(tm* time);
	static void stmpTimesyncEvent(timeval* tv);
	static void onPlayFinished();
};