set(COMPONENT_REQUIRES esp_peripherals audio_player timesync)
set(COMPONENT_PRIV_REQUIRES esp_peripherals audio_player timesync)

set(COMPONENT_ADD_INCLUDEDIRS include)
set(COMPONENT_SRCS "talking_clock.cpp")
register_component()