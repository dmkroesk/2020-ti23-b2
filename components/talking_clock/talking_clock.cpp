﻿#include "include/talking_clock.h"

TalkingClock* TalkingClock::instance;

TalkingClock::TalkingClock()
{
    this->currentTime = { 0, 0, 0 };
    //this->counter = 0;
    this->audioPlayer = AudioPlayer::getInstance();
}

void TalkingClock::init()
{
    this->timerHandle = xTimerCreate("TalkingClockTimer", pdMS_TO_TICKS(1000), pdTRUE, (void*)1, &TalkingClock::timerCallback);
    if(xTimerStart(this->timerHandle, 10) != pdPASS)
    {
		  ESP_LOGE("TalkingClock", "Cannot start timer");
    }

    ESP_LOGI("TalkingClock", "Creating FreeRTOS queue for talking clock");
    this->talkingQueue = xQueueCreate(20, sizeof(int));
    if (this->talkingQueue == NULL)
    {
		  ESP_LOGE("TalkingClock", "Error creating queue");
    }
}

void TalkingClock::start()
{
    syncTime();
    vTaskDelay(100 / portTICK_RATE_MS);
    init();
    this->audioPlayer->setOnFinishedPlayingCallback(&TalkingClock::onPlayFinished);
    this->audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    //syncTime();
}

void TalkingClock::stop()
{
    xTimerStop(this->timerHandle, 10);
    vQueueDelete(this->talkingQueue);
    //this->counter = 0;
}

void TalkingClock::update()
{
    //this->counter++;

    this->currentTime.seconds++;
    if(this->currentTime.seconds == 60)
    {
      this->currentTime.seconds = 0;
      this->currentTime.minutes++;
      if(this->currentTime.minutes == 60)
      {
        this->currentTime.minutes = 0;
        this->currentTime.hours++;
        if(this->currentTime.hours == 25)
          this->currentTime.hours = 0;
      }
    }

    if(this->OnTimerUpdate != nullptr)
        this->OnTimerUpdate(this->currentTime);

    //if(this->counter == 10)
    if(this->currentTime.seconds % 10 == 0)
    {
        buildQueue();
        startSpeaking();
        //this->counter = 0;
    }
}

void TalkingClock::buildQueue()
{
    xQueueSend(this->talkingQueue, &this->talkingClockFilesSpecial[TALKING_CLOCK_ITSNOW_INDEX], portMAX_DELAY);

    int hours = this->currentTime.hours;
    if(hours > 12)
        hours = this->currentTime.hours - 12;
    
    xQueueSend(this->talkingQueue, &this->talkingClockFilesMinutes[hours], portMAX_DELAY);
    xQueueSend(this->talkingQueue, &this->talkingClockFilesSpecial[TALKING_CLOCK_HOUR_INDEX], portMAX_DELAY);

    int minutes = this->currentTime.minutes;

    if(minutes != 0)
    {
        if((minutes % 10) != 0)
        {
            if(minutes < 15)
                xQueueSend(this->talkingQueue, &this->talkingClockFilesMinutes[minutes], portMAX_DELAY);
            else
            {
                xQueueSend(this->talkingQueue, &this->talkingClockFilesMinutes[(minutes % 10)], portMAX_DELAY);
                if(minutes > 10)
                {
                    if(minutes > 20)
                        xQueueSend(this->talkingQueue, &this->talkingClockFilesSpecial[TALKING_CLOCK_AND_INDEX], portMAX_DELAY);
                    xQueueSend(this->talkingQueue, &this->talkingClockFilesTenMinutes[(int)(minutes / 10) - 1], portMAX_DELAY);
                }
            }
        }
        else
            xQueueSend(this->talkingQueue, &this->talkingClockFilesTenMinutes[(minutes / 10) - 1], portMAX_DELAY);
    }
}

void TalkingClock::speak()
{
    const char* irl = nullptr;
    if (uxQueueMessagesWaiting(this->talkingQueue) > 0 && xQueueReceive(this->talkingQueue, &irl, portMAX_DELAY))
        this->audioPlayer->play(irl);
}

void TalkingClock::startSpeaking()
{
    TalkingClock::onPlayFinished();
}

void TalkingClock::setOnTimerUpdate(void(*onTimerUpdate)(Time time))
{
    this->OnTimerUpdate = onTimerUpdate;
}

void TalkingClock::timerCallback(TimerHandle_t xTimer)
{
    TalkingClock::getInstance()->update();
}

TalkingClock* TalkingClock::getInstance()
{
    if(TalkingClock::instance == nullptr)
        TalkingClock::instance = new TalkingClock();
    return TalkingClock::instance;
}

void TalkingClock::syncTime()
{
	sntp_sync(&TalkingClock::stmpTimesyncEvent);
}

void TalkingClock::setTime(tm* time)
{
    this->currentTime.hours = time->tm_hour + 1;
    this->currentTime.minutes = time->tm_min;
    this->currentTime.seconds = time->tm_sec;
}

void TalkingClock::stmpTimesyncEvent(timeval* tv)
{
    ESP_LOGI("TalkingClock", "Notification of a time synchronization event");
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI("TalkingClock", "The current date/time in Amsterdam is: %s", strftime_buf);

    TalkingClock::getInstance()->setTime(gmtime(&now));
}

void TalkingClock::onPlayFinished()
{
    TalkingClock::getInstance()->speak();
}