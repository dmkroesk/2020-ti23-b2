#include "stdio.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "rotary_encoder.h"
#include <iostream>

#include "esp_log.h"
#include "nvs_flash.h"

#include "buttonInput.h"
#include "project_lcd.h"
#include "audio_player.h"
#include "Menu.h"
#include "talking_clock.h"
#include "tone_detector.h"
#include "lyrat_touchbuttons.h"

#include "radio.h"

#include <sstream>
#include <string>
#include <vector>

esp_periph_set_handle_t periphHandle;
// audio_board_handle_t boardHandle;

AudioPlayer* audioPlayer;
ToneDetector* toneDetector;
Radio* radio;
TalkingClock* talkingClock;
Project_lcd* lcdPtr;
Menu* menu;
TouchButtonHandler* touchButtonHandler;

void init_flash();
void init_periph();
void deinit_periph();
void init_wifi();
void init_board_handle();

void setChannel0();
void setChannel1();
void setChannel2();
void setChannel3();
void stopAudioPlayer();

void startClock();
void stopClock();

void OnTimerUpdate(TalkingClock::Time time)
{
    std::cout << time.hours << ":" << time.minutes << ":" << time.seconds << std::endl;

    std::stringstream ss;
    ss << time.hours << ":" << time.minutes << ":" << time.seconds;
    lcdPtr->clear();
    lcdPtr->write_on_this_spot("The time is:", 0, 0);
    lcdPtr->write_on_this_spot(ss.str().c_str(), 0, 1);
}

void OnToneDetected(int frequency, float decibles)
{
    //std::cout << "[OnToneDetected] Callback Freq: " << frequency << "Hz amplitude: " << decibles << std::endl;
    switch(frequency)
    {
        case 250:{
            menu->Cycle(false);
            break;
        }
        case 500:{
            menu->Cycle(true);
            break;
        }
        case 750:{
            menu->Escape();
            break;
        }
        case 1000:{
            menu->Confirm();
            break;
        }
    }
}

extern "C" void app_main() 
{
    //---- Initialize resources ----
    init_flash();
    init_periph();
    init_wifi();
    //init_board_handle();

    // Initialize audioplayer
    audioPlayer = AudioPlayer::getInstance();
    audioPlayer->init(&periphHandle);
    //audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);

    // Initialize tonedetector
    toneDetector = ToneDetector::getInstance();
    std::vector<int> frequencies = std::vector<int>({
        250,
        500,
        750,
        1000
    });
    toneDetector->init(frequencies);
    toneDetector->setOnToneDetectedCallback(&OnToneDetected);
    toneDetector->startListening();

    // Initialize touchbuttons
    touchButtonHandler = new TouchButtonHandler(audioPlayer->getAudioBoardHandle(), &periphHandle);

    // Initialize radio
    radio = new Radio();

    // Initialize talking clock
    talkingClock = TalkingClock::getInstance();
    talkingClock->setOnTimerUpdate(&OnTimerUpdate);

    // Initialize LCD
    Project_lcd lcd(I2C_NUM_0, 0x27);
    lcdPtr = &lcd;

    // Initialize buttons
    SemaphoreHandle_t i2c_Mutex = xSemaphoreCreateMutex();
    tactilebuttons_init(&i2c_Mutex);
    //------------------------------

    // Menu setup
	Node parent(nullptr, { "Menu", "Press ok", "to continue", ""}, nullptr, nullptr);
	Node clockNode(&parent, { "Clock", "", "", "" }, nullptr, nullptr);
	Node radioNode(&parent, { "Radio", "", "", "" }, nullptr, nullptr);
    Node radioNode0(&radioNode, { "3FM Serioustalent", "", "", "" }, &setChannel0, &stopAudioPlayer);
    Node radioNode1(&radioNode, { "Hotradiohits Dab", "", "", "" }, &setChannel1, &stopAudioPlayer);
    Node radioNode2(&radioNode, { "3FM Alternative", "", "", "" }, &setChannel2, &stopAudioPlayer);
    Node radioNode3(&radioNode, { "Funx Dance BB", "", "", "" }, &setChannel3, &stopAudioPlayer);
	Node clockTimeNode(&clockNode, { "The time is: ", "", "", "" }, &startClock, &stopClock);
	menu = new Menu(&parent, &lcd);
    
    vTaskDelay(4000 / portTICK_RATE_MS);

    // QwiicTwist::qwiic_twist_t conf = {
    //     0x3F,
    //     I2C_NUM_0,
    //     18,
    //     23,
    //     GPIO_PULLUP_ENABLE,
    //     GPIO_PULLUP_ENABLE
    // };
    // QwiicTwist test = QwiicTwist(&conf, nullptr, nullptr, nullptr);
    
    while (true)
	{
        uint8_t BUTTON_LEFT = 7;
	    uint8_t BUTTON_RIGHT = 5;
	    uint8_t BUTTON_OK = 6;
	    uint8_t BUTTON_ESCAPE = 4;

        uint8_t buttons = tactilebuttons_get_pressed();

        if (!((buttons>>BUTTON_LEFT)&1)) {
            menu->Cycle(false);
        }
        if (!((buttons>>BUTTON_RIGHT)&1)) {
            menu->Cycle(true);
        }
        if (!((buttons>>BUTTON_OK)&1)) {
            menu->Confirm();
        }
        if (!((buttons>>BUTTON_ESCAPE)&1)) {
            menu->Escape();
        }
        vTaskDelay(100 / portTICK_RATE_MS);
    }

    // Deinitialize all resources
    deinit_periph();
}

void init_flash()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
}

void init_periph()
{
    ESP_LOGI("Main", "Initialize peripherals management");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    periphHandle = esp_periph_set_init(&periph_cfg);

    ESP_LOGI("Main", "Initialize and start peripherals");
    audio_board_key_init(periphHandle);
    audio_board_sdcard_init(periphHandle);
}

void deinit_periph()
{
    esp_periph_set_destroy(periphHandle);
}

void init_wifi()
{
    tcpip_adapter_init();
    ESP_LOGI("Main", "Start and wait for Wi-Fi network");
    periph_wifi_cfg_t wifi_cfg;
    wifi_cfg.ssid = CONFIG_WIFI_SSID;
    wifi_cfg.password = CONFIG_WIFI_PASSWORD;
    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    esp_periph_start(periphHandle, wifi_handle);
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);
}

// void init_board_handle()
// {
//     boardHandle = audio_board_init();
//     audio_hal_ctrl_codec(boardHandle->audio_hal, AUDIO_HAL_CODEC_MODE_BOTH, AUDIO_HAL_CTRL_START);
// }

void setChannel0()
{
    toneDetector->stopListening();
    audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    radio->setChannel(0);
}

void setChannel1()
{
    toneDetector->stopListening();
    audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    radio->setChannel(1);
}

void setChannel2()
{
    toneDetector->stopListening();
    audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    radio->setChannel(2);
}

void setChannel3()
{
    toneDetector->stopListening();
    audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    radio->setChannel(3);
}

void stopAudioPlayer()
{
    audioPlayer->stop();
    //audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    audioPlayer->setPipeline(AudioPlayer::AudioSource::NOSOURCE, AudioPlayer::AudioFileType::NOTYPE);
    toneDetector->startListening();
}

void startClock()
{
    toneDetector->stopListening();
    talkingClock->start();
}

void stopClock()
{
    talkingClock->stop();
    //audioPlayer->setPipeline(AudioPlayer::AudioSource::FILE, AudioPlayer::AudioFileType::MP3);
    audioPlayer->setPipeline(AudioPlayer::AudioSource::NOSOURCE, AudioPlayer::AudioFileType::NOTYPE);
    toneDetector->startListening();
}