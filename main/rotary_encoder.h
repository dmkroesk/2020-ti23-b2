#pragma once
#include "esp_err.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "freertos/task.h"
#include "freertos/FreeRTOS.h"

//#include "esp_peripherals.h"
#include "esp_log.h"
#include <cstdint>

#include <iostream>
#include <math.h>

#define I2C_ADDRESS 0x3F

#ifndef WRITE_BIT
    #define WRITE_BIT I2C_MASTER_WRITE
#endif

#ifndef READ_BIT
    #define READ_BIT I2C_MASTER_READ
#endif

#ifndef ACK_CHECK_EN
    #define ACK_CHECK_EN 0x1
	#define ACK_CHECK_NACK 0x0
#endif

class QwiicTwist
{
public:
    enum qwiic_twist_err_t
    {
        QWIIC_TWIST_ERR_OK = 0x00,
        QWIIC_TWIST_ERR_CONFIG = 0x01,
        QWIIC_TWIST_ERR_INSTALL = 0x02,
        QWIIC_TWIST_ERR_FAIL = 0x03
    };

    enum qwiic_twist_reg_t
    {
        QWIIC_TWIST_STATUS = 0x01,
        QWIIC_TWIST_INT_ENABLE = 0x04,
        QWIIC_TWIST_ENCODER_COUNT_LSB = 0x05,
        QWIIC_TWIST_ENCODER_COUNT_MSB = 0x06,
        QWIIC_TWIST_ENCODER_DIFFERENCE_LSB = 0x07,
        QWIIC_TWIST_ENCODER_DIFFERENCE_MSB = 0x08,
        QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_LSB = 0x09,
        QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_MSB = 0x0A,
        QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_LSB = 0x0B,
        QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_MSB = 0x0C,
        QWIIC_TWIST_LED_BRIGHTNESS_RED = 0x0D,
        QWIIC_TWIST_LED_BRIGHTNESS_GREEN = 0x0E,
        QWIIC_TWIST_LED_BRIGHTNESS_BLUE = 0x0F,
        QWIIC_TWIST_TURN_INTERRUPT_TIMEOUT_LSB = 0x16
    };

    static const size_t I2C_MASTER_TX_BUF_DISABLE = 0;
    static const size_t I2C_MASTER_RX_BUF_DISABLE = 0;
    static const int INTR_FLAGS = 0;
	
	struct qwiic_twist_t
	{
		uint8_t i2c_addr;
		i2c_port_t port;
		uint8_t sda_pin;
		uint8_t scl_pin;
		gpio_pullup_t sda_pullup_en;
		gpio_pullup_t scl_pullup_en;
	};

private:
    qwiic_twist_t* qwiic_twist;
    SemaphoreHandle_t xMutex;
    int twistCount;

    void(*onButtonPressed)(void);   //Called when button of rotary encoder is pressed
    void(*onMovedLeft)(int);        //Called when rotary encoder moved to the left and passes the ammount of twists to the left
    void(*onMovedRight)(int);       //Called when rotary encoder moved to the right and passes the ammount of twists to the right

public:
    QwiicTwist(qwiic_twist_t* qwiic_twist, void(*onButtonPressed)(void), void(*onMovedLeft)(int), void(*onMovedRight)(int));

    void setColor(uint8_t red, uint8_t green, uint8_t blue);
    void setRed(uint8_t red);
    void setGreen(uint8_t green);
    void setBlue(uint8_t blue);

    void runTask();

private:
    qwiic_twist_err_t init();

    bool isPressed();
    bool isClicked();
    bool isMoved();
    int getTwistCount();

    qwiic_twist_err_t writeRegister(qwiic_twist_reg_t reg, uint8_t value);
    qwiic_twist_err_t writeRegister16(qwiic_twist_reg_t reg, uint16_t value);
    qwiic_twist_err_t writeRegister24(qwiic_twist_reg_t reg, uint32_t value);
    qwiic_twist_err_t readRegister(qwiic_twist_reg_t reg, uint8_t* data);
    qwiic_twist_err_t readRegister16(qwiic_twist_reg_t reg, uint16_t* data);
};