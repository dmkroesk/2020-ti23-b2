#include "rotary_encoder.h"

#include "driver/gpio.h"

#include <iostream>

QwiicTwist::QwiicTwist(qwiic_twist_t* qwiic_twist, void(*onButtonPressed)(void), void(*onMovedLeft)(int), void(*onMovedRight)(int))
{
	this->qwiic_twist = qwiic_twist;
	this->xMutex = xSemaphoreCreateMutex();
	//ESP_ERROR_CHECK(init());

	this->onButtonPressed = onButtonPressed;
	this->onMovedLeft = onMovedLeft;
	this->onMovedRight = onMovedRight;

	//this->twistCount = getTwistCount();

	//writeRegister(QWIIC_TWIST_INT_ENABLE, 0x01);
	//writeRegister16(QWIIC_TWIST_TURN_INTERRUPT_TIMEOUT_LSB, 10);
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::init()
{
	esp_err_t ret;

	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = (gpio_num_t)this->qwiic_twist->sda_pin;
	conf.scl_io_num = (gpio_num_t)this->qwiic_twist->scl_pin;
	conf.sda_pullup_en = this->qwiic_twist->sda_pullup_en;
	conf.scl_pullup_en = this->qwiic_twist->scl_pullup_en;
	conf.master.clk_speed = 20000;

		//Fix for input lag.
    i2c_set_timeout(I2C_NUM_0, 20000);

		//Fix for input lag.
    i2c_set_timeout(I2C_NUM_0, 20000);

	//this->qwiic_twist->port
	ret = i2c_param_config(I2C_NUM_0, &conf);

	if (ret != ESP_OK) {
		ESP_LOGE("QwiicTwist", "PARAM CONFIG FAILED");
		return QWIIC_TWIST_ERR_CONFIG;
	}
	ESP_LOGV("QwiicTwist", "PARAM CONFIG done");

	return QWIIC_TWIST_ERR_OK;
}

bool QwiicTwist::isPressed()
{
	uint8_t status = 0;
	readRegister(QWIIC_TWIST_STATUS, &status);
	bool pressed = status & (1 << 1);

	writeRegister(QWIIC_TWIST_STATUS, status & ~(1 << 1));
	return pressed;
}

bool QwiicTwist::isClicked()
{
	uint8_t status = 0;
	readRegister(QWIIC_TWIST_STATUS, &status);
	bool clicked = status & (1 << 2);

	writeRegister(QWIIC_TWIST_STATUS, status & ~(1 << 2));
	return clicked;
}

bool QwiicTwist::isMoved()
{
	uint8_t status = 0;
	readRegister(QWIIC_TWIST_STATUS, &status);
	bool moved = status & (1 << 0);

	writeRegister(QWIIC_TWIST_STATUS, status & ~(1 << 0));
	return moved;
}

int QwiicTwist::getTwistCount()
{
	uint16_t count = 0;
	readRegister16(QWIIC_TWIST_ENCODER_DIFFERENCE_LSB, &count);
	writeRegister16(QWIIC_TWIST_ENCODER_DIFFERENCE_LSB, 0);

	return count;
}

void QwiicTwist::runTask()
{
//	while (1)
//	{
	
		// __CR__ [PSMG]: Waarom roep je hier los alle statussen op, want alle statussen (pressed, clicked, moved) kunnen in 1 register uitgelezen worden
		// __CR__ [PSMG]: 
		if (isPressed() && this->onButtonPressed != nullptr)
			this->onButtonPressed();

		if (isMoved())
		{
			int count = getTwistCount();
			int difference = count - getTwistCount();
            ESP_LOGI("rotary_encoder: ", "Difference is: %d.", difference);
			this->twistCount = count;
			if (difference > 0 && this->onMovedLeft != nullptr)
			{
				//setColor(0, 255, 0);
				setColor(0,getTwistCount(),0);
				this->onMovedLeft(abs(difference));
			}
			else if(this->onMovedRight != nullptr)
			{
			    //setColor(0,0,255);
				setColor(0, 0, getTwistCount());
				this->onMovedRight(abs(difference));
			}
		}
//	}
}

void QwiicTwist::setColor(uint8_t red, uint8_t green, uint8_t blue)
{
	writeRegister24(QWIIC_TWIST_LED_BRIGHTNESS_RED, (uint32_t)red << 16 | (uint32_t)green << 8 | blue);
}

void QwiicTwist::setRed(uint8_t red)
{
	writeRegister(QWIIC_TWIST_LED_BRIGHTNESS_RED, red);
}

void QwiicTwist::setGreen(uint8_t green)
{
	writeRegister(QWIIC_TWIST_LED_BRIGHTNESS_GREEN, green);
}

void QwiicTwist::setBlue(uint8_t blue)
{
	writeRegister(QWIIC_TWIST_LED_BRIGHTNESS_BLUE, blue);
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::writeRegister(qwiic_twist_reg_t reg, uint8_t value)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE("QwiicTwist", "ERROR: unable to write to register %d", ret);
		return QWIIC_TWIST_ERR_FAIL;
	}
	vTaskDelay(100 / portTICK_RATE_MS);

	return QWIIC_TWIST_ERR_OK;
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::writeRegister16(qwiic_twist_reg_t reg, uint16_t value)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)reg, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value & 0xFF, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value >> 8, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE("QwiicTwist", "ERROR: unable to write to register %d", ret);
		return QWIIC_TWIST_ERR_FAIL;
	}
	vTaskDelay(100 / portTICK_RATE_MS);

	return QWIIC_TWIST_ERR_OK;
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::writeRegister24(qwiic_twist_reg_t reg, uint32_t value)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)reg, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value >> 16, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value >> 8, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, value & 0xFF, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK)
	{
		ESP_LOGE("QwiicTwist", "ERROR: unable to write to register %d", ret);
		return QWIIC_TWIST_ERR_FAIL;
	}
	vTaskDelay(100 / portTICK_RATE_MS);

	return QWIIC_TWIST_ERR_OK;
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::readRegister(qwiic_twist_reg_t reg, uint8_t* data)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)reg, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE("QwiicTwist", "ERROR: unable to write address %02x to read reg %02x %d", this->qwiic_twist->i2c_addr, (uint8_t)reg, ret);
		return QWIIC_TWIST_ERR_FAIL;
	}

	//ESP_LOGI("QwiicTwist", "Read first stage: %d", ret);

	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | I2C_MASTER_READ, ACK_CHECK_NACK);  // __CR__ [PSMG]: Laatste keer hoef je dit niet te doen, dan een NACK
	i2c_master_read_byte(cmd, data, (i2c_ack_type_t)1);
	ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK)
	{
		ESP_LOGE("QwiicTwist", "ERROR: unable to write address %02x to read reg %02x %d", this->qwiic_twist->i2c_addr, (uint8_t)reg, ret);
		return QWIIC_TWIST_ERR_FAIL;
	}

	//ESP_LOGI("QwiicTwist", "Read second stage: %d", ret);
	vTaskDelay(100 / portTICK_RATE_MS);

	return QWIIC_TWIST_ERR_OK;
}

QwiicTwist::qwiic_twist_err_t QwiicTwist::readRegister16(qwiic_twist_reg_t reg, uint16_t* data)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, (uint8_t)reg, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK)
	{
		ESP_LOGE("QwiicTwist", "ERROR: unable to write address %02x to read reg %02x %d", this->qwiic_twist->i2c_addr, (uint8_t)reg, ret);
		return QWIIC_TWIST_ERR_FAIL;
	}

	//ESP_LOGI("QwiicTwist", "Read first stage: %d", ret);

	uint8_t lsb = 0x00;
	uint8_t msb = 0x00;

	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	// __CR__ [PSMG]: Hier gaat het fout, want je moet wel een ACK doen, anders gaat het fout
	i2c_master_write_byte(cmd, (this->qwiic_twist->i2c_addr << 1) | I2C_MASTER_READ, ACK_CHECK_NACK);
	i2c_master_read_byte(cmd, &lsb, (i2c_ack_type_t)1);
	i2c_master_read_byte(cmd, &msb, (i2c_ack_type_t)1); // __CR__ [PSMG]: Laatste keer hoef je dit niet te doen, dan een NACK
	ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK)
	{
		ESP_LOGE("QwiicTwist", "ERROR: unable to write address %02x to read reg %02x %d", this->qwiic_twist->i2c_addr, (uint8_t)reg, ret);
		return QWIIC_TWIST_ERR_FAIL;
	}

	*data = ((uint16_t)msb << 8 | lsb);

	//ESP_LOGI("QwiicTwist", "Read second stage: %d", ret);
	vTaskDelay(100 / portTICK_RATE_MS);

	return QWIIC_TWIST_ERR_OK;
}
